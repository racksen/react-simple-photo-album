import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from './src/components/Header';
import AlbumList from "./src/components/AlbumList";

export default App = () => {
  const { containerStyle } = styles;
  return (
    <View style={{flex: 1}}>
      <Header title='Albums!' />
      <AlbumList></AlbumList>
    </View>
  )
}

const styles = StyleSheet.create({
 
});


// export default class App extends Component {
//   render() { 
//     return (
//       <View style={styles.containerStyle}>
//         <Text>Open up App.js to start working on your app!</Text>
//         <Text>Changes you make will automatically reload.</Text>
//         <Text>Shake your phone to open the developer menu.</Text>
//       </View>
//     );
//   }
// }

import React, { Component } from 'react';
import { View, StyleSheet } from "react-native";

const CardSection = (props) => {
  const { containerViewStyle } = styles;
  return (
    <View style={containerViewStyle}>
      {props.children}
    </View>
  );
}

const styles = StyleSheet.create({
  containerViewStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: "#fff",
    justifyContent: "flex-start",
    flexDirection: "row",
    borderColor: '#ddd',
    position: "relative",
  }
});

export default CardSection;

import React, { Component } from 'react';
import { View, StyleSheet } from "react-native";

const Card = (props) => {
  const { containerViewStyle } = styles;
  return (
    <View style={containerViewStyle}>
      {props.children}
    </View>
  );
}

const styles = StyleSheet.create({
  containerViewStyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 2,
    position: "relative",
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  }
});

export default Card;

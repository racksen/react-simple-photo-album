import React, { Component } from 'react';
import { StyleSheet, Text, View , Image, Linking} from "react-native";
import Card from "./Card";
import CardSection from "./CardSection";
import Button from "./Button";

class AlbumDetail extends Component {
  state = {}

  onPress = (url) => {
    Linking.openURL(url).catch(err =>
      console.error("An error occurred", err)
    );
  }

  render() {
    const { headerContentStyle, thumbnailContainerStyle, thumbnailStyle, headerTitleStyle, imageStyle } = styles;
    const { title, artist, thumbnail_image, image, url } = this.props.album;
    
    return <Card>
        <CardSection>
          <View style={thumbnailContainerStyle}>
            <Image source={{ uri: thumbnail_image }} style={thumbnailStyle} />
          </View>
          <View style={headerContentStyle}>
            <Text style={headerTitleStyle}> {title}</Text>
            <Text> {artist}</Text>
          </View>
        </CardSection>
        <CardSection>
          <Image source={{ uri: image }} style={imageStyle} />
        </CardSection>
        <CardSection>
          <Button onPress={() => this.onPress(url)}>
            Buy Now
          </Button>
        </CardSection>
      </Card>;
  }
}

const styles = StyleSheet.create({
  headerContentStyle: {
    flexDirection: "column",
    justifyContent: "space-around"
  },
  headerTitleStyle: {
    fontSize: 18,
    fontWeight: "bold"
  },
  thumbnailContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  imageStyle: {
    height: 400,
    flex: 1,
    width: null
  }
});

export default AlbumDetail;